package com.builders.crudapi.domain.dto;


import java.time.LocalDate;

public class ClientRequest {

    public ClientRequest() {
    }

    public ClientRequest(String name, String personalDocument, LocalDate birthDate) {
        this.name = name;
        this.personalDocument = personalDocument;
        this.birthDate = birthDate;
    }

    private String name;

    private String personalDocument;

    private LocalDate birthDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalDocument() {
        return personalDocument;
    }

    public void setPersonalDocument(String personalDocument) {
        this.personalDocument = personalDocument;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

}
