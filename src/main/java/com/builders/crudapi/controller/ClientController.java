package com.builders.crudapi.controller;

import com.builders.crudapi.domain.dto.ClientRequest;
import com.builders.crudapi.domain.dto.ClientResponse;
import com.builders.crudapi.domain.exception.ResourceNotFoundException;
import com.builders.crudapi.domain.vo.Client;
import com.builders.crudapi.service.ClientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/v1/clients")
public class ClientController {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    private ClientService clientService;

    @RequestMapping
    public ResponseEntity<Page<ClientResponse>> list(
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", required = false, defaultValue = "10") int size,
            @RequestParam(required = false) String personalDocument,
            @RequestParam(required = false) String name) {

        Page<Client> clientsPage = clientService.findAllBy(personalDocument, name, page, size);

        List<ClientResponse> clientsDTO = clientsPage
                .map(c -> modelMapper.map(c, ClientResponse.class))
                .stream()
                .collect(Collectors.toList());

        Page<ClientResponse> clientPage = new PageImpl<ClientResponse>(clientsDTO, clientsPage.getPageable(), clientsPage.getTotalElements());

        return ResponseEntity.ok(clientPage);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<ClientResponse> findById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        Client client = clientService.findById(id);
        return ResponseEntity.ok(modelMapper.map(client, ClientResponse.class));
    }

    @PostMapping
    public ResponseEntity<ClientResponse> post(@RequestBody ClientRequest postClient) {
        Client client = clientService.save(modelMapper.map(postClient, Client.class));

        return ResponseEntity.status(HttpStatus.CREATED).body(modelMapper.map(client, ClientResponse.class));
    }

    @PutMapping("{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody ClientRequest postClient) throws ResourceNotFoundException {
        clientService.update(id, modelMapper.map(postClient, Client.class));

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("{id}")
    public ResponseEntity<?> patch(@PathVariable Long id, @RequestBody ClientRequest postClient) throws ResourceNotFoundException {
        clientService.update(id, modelMapper.map(postClient, Client.class));

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        clientService.deleteById(id);

        return ResponseEntity.noContent().build();
    }

}
