package com.builders.crudapi.repository;

import com.builders.crudapi.domain.vo.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
    @Query("SELECT c FROM Client c WHERE " +
            " (:personalDocument IS NOT NULL AND c.personalDocument = :personalDocument) " +
            " OR (:name IS NOT NULL AND LOWER(c.name) like %:name%) "
    )
    Page<Client> findAllBy(@Param("personalDocument") String personalDocument, @Param("name") String name, Pageable pageable);

}