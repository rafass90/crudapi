package com.builders.crudapi.service;

import com.builders.crudapi.domain.exception.ResourceNotFoundException;
import com.builders.crudapi.domain.vo.Client;
import com.builders.crudapi.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client save(Client client) {
        return clientRepository.save(client);
    }

    public Client findById(Long id) throws ResourceNotFoundException {
        return clientRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
    }

    public Page<Client> findAllBy(String personalDocument, String name, int page, int size) {
        final PageRequest pageRequest = PageRequest.of(page, size, ASC, "name");

        return clientRepository.findAllBy(personalDocument, name, pageRequest);
    }

    public Client update(Long id, Client client) throws ResourceNotFoundException {
        final Client oldClient = clientRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);

        client.setId(id);

        if (client.getBirthDate() == null)
            client.setBirthDate(oldClient.getBirthDate());

        if (client.getPersonalDocument() == null)
            client.setPersonalDocument(oldClient.getPersonalDocument());

        if (client.getName() == null)
            client.setName(oldClient.getName());

        return save(client);
    }

    public void deleteById(Long id) {
        clientRepository.deleteById(id);
    }

}
