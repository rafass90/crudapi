package com.builders.crudapi.configuration;

import com.builders.crudapi.domain.dto.ClientResponse;
import com.builders.crudapi.domain.vo.Client;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ModelMapperTest {

    @Autowired
    private ModelMapper modelMapper;

    @Test
    void assertInjection() {
        assertThat(modelMapper).isNotNull();
    }

    @Test
    void givenClient_shouldCreateClientResponse() {
        LocalDate date = LocalDate.now();
        Client client = new Client("name test", "12345678901", date);

        ClientResponse clientResponse = modelMapper.map(client, ClientResponse.class);

        assertThat(clientResponse.getName()).isEqualTo("name test");
        assertThat(clientResponse.getPersonalDocument()).isEqualTo("12345678901");
        assertThat(clientResponse.getAge()).isEqualTo(0);

    }
}