package com.builders.crudapi.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseSetups;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@AutoConfigureWebTestClient
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetups({
        @DatabaseSetup("/dataset/clear_db.xml"),
        @DatabaseSetup("/dataset/insert_client_db.xml")
})
public class ClientControllerDeleteTest {

    @Autowired
    private WebTestClient webClient;

    @Test
    void deleteExistingClient_shouldReturn204() {
        webClient
                .delete().uri("api/v1/clients/1")
                .exchange()
                .expectStatus().isNoContent();
    }

    @Test
    void deleteNotExistingClient_shouldReturn200() {
        webClient
                .get().uri("api/v1/clients/111")
                .exchange()
                .expectStatus().isNotFound();
    }
}
