package com.builders.crudapi.controller;

import com.builders.crudapi.domain.dto.ClientRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseSetups;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@AutoConfigureWebTestClient
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetups({
        @DatabaseSetup("/dataset/clear_db.xml"),
        @DatabaseSetup("/dataset/insert_client_db.xml")
})
public class ClientControllerPatchTest {

    @Autowired
    private WebTestClient webClient;

    @Test
    void patchExistingClient_shouldReturn201() {
        final ClientRequest request =
                new ClientRequest("New Name Client", null, null);

        webClient
                .patch().uri("api/v1/clients/1")
                .bodyValue(request)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody();
    }

    @Test
    void patchNotExistingClient_shouldReturn404() {
        final ClientRequest request =
                new ClientRequest(null, "New Name Client", null);

        webClient
                .patch().uri("/clientes/2").bodyValue(request)
                .exchange()
                .expectStatus().isNotFound();
    }


}
