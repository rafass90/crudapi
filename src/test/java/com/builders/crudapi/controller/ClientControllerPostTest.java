package com.builders.crudapi.controller;

import com.builders.crudapi.domain.dto.ClientRequest;
import com.builders.crudapi.domain.dto.ClientResponse;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseSetups;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@AutoConfigureWebTestClient
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetups({
        @DatabaseSetup("/dataset/clear_db.xml"),
        @DatabaseSetup("/dataset/insert_client_db.xml")
})
public class ClientControllerPostTest {

    @Autowired
    private WebTestClient webClient;

    @Test
    void postClient_shouldReturn201() {
        ClientRequest request =
                new ClientRequest("New Client", "12345678901", LocalDate.now());

        webClient
                .post().uri("api/v1/clients")
                .bodyValue(request)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    void postClient_shouldReturnClientResponse() {

        String clientName = "New Client";
        String personalDocument = "12345678901";
        LocalDate birthDate = LocalDate.now().minusYears(20);

        ClientRequest request =
                new ClientRequest(clientName, personalDocument, birthDate);

        ClientResponse response = webClient
                .post().uri("api/v1/clients")
                .bodyValue(request)
                .exchange()
                .expectBody(ClientResponse.class).returnResult().getResponseBody();

        assertThat(response.getId(), is(greaterThan(0L)));
        assertThat(response.getName(), is(equalTo(clientName)));
        assertThat(response.getPersonalDocument(), is(equalTo(personalDocument)));
        assertThat(response.getAge(), is(20));

    }

}
