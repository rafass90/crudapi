Desafio:
Desenvolva um projeto Java com Spring Boot (utilizando qualquer modulo que achar necessário), que possua uma api REST com CRUD de Cliente (id, nome, cpf, dataNascimento). O CRUD deve possuir uma api de GET, POST, DELETE, PATCH e PUT.
A api de GET deve aceitar query strings para pesquisar os clientes por CPF e nome. Também é necessário que nessa api os clientes voltem paginados e que possua um campo por cliente com a idade calculado dele considerando a data de nascimento.
O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL).
O projeto deve estar em um repositório no BitBucket e na raiz do projeto deve conter um Postman para apreciação da api.


Ferramentas utilizadas no desenvolvimento da api:
* idea
* JDK8
* Postman
* Docker
* Gradle

O arquivo Postman está disponível na raiz do projeto, contém duas variáveis:
crudapi.postman_collection.json
1 - crud_url (padrão localhost, conforme configurado no application.properties)
2 - crud_port (padrão 8080, conforme configurado no application.properties)


## Para executar o banco de dados, executar o comando docker abaixo
Requisitos: docker instalado

``docker run -d -p 9001:9001 --name hsqldb datagrip/hsqldb:2.3.4``
 
 
## Para executar o projeto localmente, inserir via linha de comando no diretrio raiz do projeto Requisitos: JDK8, Gradle
Comando: ``./gradle build`` para compilar a aplicação.

Comando: ``./gradle bootRun`` para iniciar o tomcat.
